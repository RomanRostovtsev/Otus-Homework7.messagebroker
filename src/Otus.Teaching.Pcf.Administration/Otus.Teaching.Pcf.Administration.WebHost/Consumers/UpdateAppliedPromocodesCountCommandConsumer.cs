﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Common;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class UpdateAppliedPromocodesCountCommandConsumer : IConsumer<UpdateAppliedPromocodesCountCommand>
    {

        private readonly AppliedPromocodesUpdater _updater;
        public UpdateAppliedPromocodesCountCommandConsumer(AppliedPromocodesUpdater updater)
        {
            _updater = updater;
        }

        async Task IConsumer<UpdateAppliedPromocodesCountCommand>.Consume(ConsumeContext<UpdateAppliedPromocodesCountCommand> context)
        {
            try
            {
                await _updater.UpdateAppliedPromocodesCount(context.Message.PartnerManagerId);
            }
            catch (ArgumentException)
            {
            }
        }
    }
}
