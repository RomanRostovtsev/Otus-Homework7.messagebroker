﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class AppliedPromocodesUpdater
    {
        private readonly IRepository<Employee> _employeesRepository;
        public AppliedPromocodesUpdater(IRepository<Employee> employeesRepository)
        {
            _employeesRepository = employeesRepository;
        }

        public async Task UpdateAppliedPromocodesCount(Guid employeeId)
        {
            var employee = await _employeesRepository.GetByIdAsync(employeeId);

            if (employee == null)
                throw new ArgumentException("No employee with such an id.");

            employee.AppliedPromocodesCount++;

            await _employeesRepository.UpdateAsync(employee);

        }
    }
}
