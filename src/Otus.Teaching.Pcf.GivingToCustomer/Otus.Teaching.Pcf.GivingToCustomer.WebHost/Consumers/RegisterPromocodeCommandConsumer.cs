﻿using MassTransit;
using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class RegisterPromocodeCommandConsumer : IConsumer<RegisterPromocodeCommand>
    {
        private readonly PromocodesRegistrator _registrator;

        public RegisterPromocodeCommandConsumer(PromocodesRegistrator registrator)
        {
            _registrator= registrator;
        }

        Task IConsumer<RegisterPromocodeCommand>.Consume(ConsumeContext<RegisterPromocodeCommand> context)
        {
            var promocodeInfo = new PromocodeInfo()
            {
                PromocodeId = context.Message.PromoCodeId,
                Promocode = context.Message.PromoCode,
                ServiceInfo = context.Message.ServiceInfo,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate,
                PartnerId = context.Message.PartnerId,
                PreferenceId = context.Message.PreferenceID
            };
            return _registrator.RegisterPromocode(promocodeInfo);
        }
    }
}
