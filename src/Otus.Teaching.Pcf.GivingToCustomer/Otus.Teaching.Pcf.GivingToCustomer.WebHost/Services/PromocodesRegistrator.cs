﻿using Otus.Teaching.Pcf.Common;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Services
{
    public class PromocodeInfo
    {
        public string Promocode;
        public Guid PromocodeId;
        public string ServiceInfo;
        public Guid PartnerId;
        public DateTime BeginDate;
        public DateTime EndDate;
        public Guid PreferenceId;
    }

    public class PromocodesRegistrator
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public PromocodesRegistrator(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _preferencesRepository= preferencesRepository;
            _customersRepository= customersRepository;
            _promoCodesRepository= promoCodesRepository;
        }

        public async Task RegisterPromocode(PromocodeInfo promocodeInfo)
        {
            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(promocodeInfo.PreferenceId);

            if (preference == null)
            {
                throw new ArgumentException(nameof(promocodeInfo.PreferenceId));
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(promocodeInfo, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

        }
    }
}
