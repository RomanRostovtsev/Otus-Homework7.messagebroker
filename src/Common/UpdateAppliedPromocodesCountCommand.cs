using System;

namespace Otus.Teaching.Pcf.Common
{

    public class UpdateAppliedPromocodesCountCommand
    {
        public Guid PartnerManagerId { get; set; }
    }

}