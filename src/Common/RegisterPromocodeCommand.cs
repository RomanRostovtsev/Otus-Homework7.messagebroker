using System;

namespace Otus.Teaching.Pcf.Common
{

    public class RegisterPromocodeCommand
    {
        public Guid PromoCodeId { get; set; }
        public string PromoCode { get; set; }
        public string ServiceInfo { get; set; }

        public Guid PartnerId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid PreferenceID { get; set; }


    }

}